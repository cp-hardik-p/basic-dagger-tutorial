package com.example.basicdaggertutorial

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class App: Application() {

    override fun onCreate() {
        super.onCreate()

        component = DaggerAppComponent
            .builder()
            .build()
    }
}

lateinit var component: AppComponent