package com.example.basicdaggertutorial

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class UserRepositoryImpl(private val api: Api) : UserRepository {
    private val compositeDisposable = CompositeDisposable()
    override fun getUser(
        username: String,
        onSuccess: (user: User) -> Unit,
    ) {
        api.getUser(username)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onError = { it.printStackTrace() },
            ) {
                onSuccess.invoke(it)
            }.addTo(compositeDisposable)
    }
}