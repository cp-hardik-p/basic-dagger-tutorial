package com.example.basicdaggertutorial

interface UserRepository {
    fun getUser(username: String, onSuccess: (user: User)-> Unit)
}