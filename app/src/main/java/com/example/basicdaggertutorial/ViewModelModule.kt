package com.example.basicdaggertutorial

import dagger.Module
import dagger.Provides

@Module
class ViewModelModule {

    @Provides
    fun providesMainViewModelFactory(repository: UserRepository): MainViewModelFactory{
        return MainViewModelFactory(repository)
    }

}