package com.example.basicdaggertutorial

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface Api {
    @GET("users/{user}")
   fun getUser(@Path("user")user: String) : Observable<User>

}