package com.example.basicdaggertutorial

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel(private val userRepository: UserRepository) : ViewModel() {

    val fullName = MutableLiveData<String>()

  //  private var userRepository: UserRepository = UserRepositoryImpl(api)

    fun searchUser(username: String) {

        userRepository.getUser(username)
            { user -> fullName.value = user.name }
    }
}